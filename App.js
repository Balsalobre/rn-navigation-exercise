import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

//Screens
import { DetailScreen, UsersScreen, PostsScreen } from './screens/index';

const AppNavigator = createStackNavigator({
  Users: {
    screen: UsersScreen,
  },
  Posts: {
    screen: PostsScreen,
  },
  Detail: {
    screen: DetailScreen,
  }
}, {
  initialRouteName: 'Users',
  defaultNavigationOptions: {

  }
});

export default createAppContainer(AppNavigator);
