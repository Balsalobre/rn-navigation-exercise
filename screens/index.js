import UsersScreen from './Users';
import PostsScreen from './Posts';
import DetailScreen from './Detail';

export { UsersScreen, PostsScreen, DetailScreen }; 